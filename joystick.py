from sense_hat import SenseHat
from threading import Thread

sense = SenseHat()

def joystick_state():
    evt = sense.stick.wait_for_event()
    if evt.action=='pressed': # consume release event
        sense.stick.wait_for_event()
    return evt.direction

class JoystickThread(Thread):
    def __init__(self, callback):
        super().__init__(name='JoystickDaemon', daemon=True)
        self.callback=callback
    
    def run(self):
        while True:
            evt = joystick_state()
            self.callback(evt)
