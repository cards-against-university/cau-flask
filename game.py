import random

class GameError(Exception):
    pass

class Player:
    def __init__(self, username):
        self.username=username
        self.score=0
        self.card=None
        self.czar=False

# global game state
state='idle'
players=[]
selectedCard=None
inputs=[]

# input daemon
def inputCallback(evt):
    global inputs
    inputs.append(evt)
def getInput():
    global inputs
    if len(inputs)==0:
        return None
    evt=inputs[0]
    inputs.remove(evt)
    return evt
def pushInput(evt):
    global inputs
    inputs.append(evt)

try:
    from joystick import JoystickThread
    JoystickThread(inputCallback).start()
except:
    pass

def _getPlayer(username):
    global players
    for player in players:
        if player.username==username:
            return player
    raise ValueError(username+" not found")

# load all cards from disk
whiteCards, whiteCardIdx=([], 0)
blackCards, blackCardIdx=([], 0)
with open("whitecards.txt", "r") as fd:
    whiteCards=[card.replace('\n', '') for card in fd.readlines() if card!='\n']
    whiteCardIdx=0
    fd.close()
with open("blackcards.txt", "r") as fd:
    blackCards=[card.replace('\n', '') for card in fd.readlines() if card!='\n']
    blackCardIdx=0
    fd.close()

# draw cards
random.shuffle(whiteCards)
random.shuffle(blackCards)
def resetWhiteCards():
    global whiteCards
    global whiteCardIdx
    
    random.shuffle(whiteCards)
    whiteCardIdx=0

def getWhiteCard():
    global whiteCards
    global whiteCardIdx
    
    card=whiteCards[whiteCardIdx]
    whiteCardIdx+=1
    if whiteCardIdx==len(whiteCards):
        random.shuffle(whiteCards)
        whiteCardIdx=0
    return card

def resetBlackCards():
    global blackCards
    global blackCardIdx
    
    random.shuffle(blackCards)
    blackCardIdx=0

def getBlackCard():
    global blackCards
    global blackCardIdx
    
    card=blackCards[blackCardIdx]
    blackCardIdx+=1
    if blackCardIdx==len(blackCards):
        random.shuffle(blackCards)
        blackCardIdx=0
    return card

# operate the game
def start():
    global state
    
    if state!='idle':
        raise GameError("Game is already started")
    state='playing'
    try:
        from countdown import CountdownThread
        CountdownThread(0, 10, choiceMode).start()
    except:
        pass

def choiceMode():
    global state
    if state!='playing':
        raise GameError("Game is not currently running")
    state='selecting'


def reset():
    global state
    global selectedCard
    global players
    
    state='idle'
    selectedCard=None
    resetWhiteCards()
    for player in players:
        player.card=None

def join(username):
    global state
    global players
    
    if state!='idle':
        raise GameError("Game is already started")
    players.append(Player(username))

def leave(username):
    global players
    
    player=_getPlayer(username)
    if player.czar:
        raise GameError("User is the card czar")
    players.remove(player)

def play(user, card):
    global state
    
    if state!='playing':
        raise GameError("Not currently playing")
    
    player=_getPlayer(user)
    if player.card:
        raise GameError("User has already played a card")
    if player.czar:
        raise GameError("User is the card czar")
    player.card=card

def getState():
    global state
    return state

def getPlayers():
    global players
    return players

def getPlayedCards():
    global players
    return list(filter(lambda a: a,map(lambda a: a.card, players)))

def getSelectedCard():
    global selectedCard
    return selectedCard

def selectCard(card):
    global state
    global players
    global selectedCard
    
    if state!='selecting':
        raise GameError("Game is not running")
    
    found=None
    for player in players:
        if player.card==card:
            found=player
            player.score+=1
    
    if not found:
        raise GameError("Card not found")
    
    selectedCard=card
    state='ended'
