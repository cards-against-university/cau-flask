from sense_hat import SenseHat
import time
from threading import Thread

def start_round_countdown(timeC,countD):
    """
    timeC : int
    countD : int
    post-cond: does a background countdown of timeC seconds and a front-end countdown of countD seconds
                overall the countdown will be (timeC+countD)
    """

    sense = SenseHat()
    #/srv/flask
    time.sleep(int(timeC))
    sense.show_message("Rounds end in " + str(countD) + " seconds", text_colour=[0, 0, 255])

    for i in range(int(countD), 0-1, -1):
        sense.show_message(str(i), text_colour=[0, 0, 255])


    sense.show_message("Time out you fuckers", text_colour=[0, 0, 255])

class CountdownThread(Thread):
    def __init__(self, timeC=0, countD=20, callback=None):
        super().__init__(name='Countdown', daemon=False)
        self.timeC=timeC
        self.countD=countD
        self.callback=callback

    def run(self):
        start_round_countdown(self.timeC, self.countD)
        if self.callback:
            self.callback()

