from flask import Flask, request
import json

import game
app=Flask(__name__)

@app.route('/api')
def route_root():
    return "It works!"

@app.route('/api/ping')
def route_ping():
    return "pong"

@app.route('/api/blackcard')
def route_blackCard():
    return game.getBlackCard()

@app.route('/api/whitecard')
def route_whiteCard():
    return game.getWhiteCard()

@app.route('/api/temp')
def route_temp():
    return subprocess.run(['vcgencmd', 'measure_temp'], capture_output=True).stdout

@app.route('/api/state')
def route_state():
    return game.getState()

@app.route('/api/start')
def route_start():
    game.start()
    return "OK"

@app.route('/api/reset')
def route_reset():
    game.reset()
    return "OK"

@app.route('/api/join', methods=['POST'])
def route_join():
    game.join(request.json['username'])
    return "OK"

@app.route('/api/leave', methods=['POST'])
def route_leave():
    game.leave(request.json['username'])
    return "OK"

@app.route('/api/play', methods=['POST'])
def route_play():
    game.play(request.json['username'], request.json['card'])
    return "OK"

@app.route('/api/players')
def route_players():
    return json.dumps(list(map(lambda a: {'username': a.username, 'czar': a.czar, 'card': a.card, 'score': a.score}, game.getPlayers())))

@app.route('/api/cards')
def route_cards():
    return json.dumps(game.getPlayedCards())

@app.route('/api/selectedCard')
def route_selectedCard():
    return game.getSelectedCard()

@app.route('/api/selecting')
def route_selecting():
    game.choiceMode()
    return "OK"

@app.route('/api/select', methods=['POST'])
def route_select():
    game.selectCard(request.json['card'])
    return "OK"

@app.route('/api/input', methods=['GET'])
def route_input():
    evt = game.getInput()
    return evt or "none"

@app.route('/api/input', methods=['POST'])
def route_post_input():
    game.pushInput(request.json['input'])
    return "OK"
